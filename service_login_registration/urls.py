from django.contrib import admin
from django.conf.urls import url
from service_login_registrationapp import views
urlpatterns = [
    url('admin/', admin.site.urls),
    url(r'^service/', views.serform ),
    url(r'^reg/', views.registration ),
    url(r'^$', views.rlogin ),
    url(r'^forgotpassword/', views.forgpass ),
    url(r'^admindata/', views.admindataa),
    url(r'^otp/',views.otpverify),
    url(r'^uppass/',views.updatepass),
    # url(r'^forotp/',views.forotpverify),
    # url(r'logout/', views.logoutt ),
 ]
