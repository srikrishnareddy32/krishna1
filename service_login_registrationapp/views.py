from django.shortcuts import render,redirect
from .models import userdata,registrationdata
from .forms import serviceform,loginform,forgotpassword,registrationform,otpform,updatepassword,forotpform
from django.http.response import HttpResponse
# from django.contrib.auth.decorators import login_required
# from django.contrib.auth import authenticate,login,logout
# from django.urls import reverse
import random


otp=random.randint(1000,2000)
def registration(request):
    global mobile
    if request.method=='POST':
        regform=registrationform(request.POST)
        if regform.is_valid():
            mobile=request.POST.get('mobile','')
            dat=registrationdata.objects.filter(mobile=mobile)
            if dat:
                return HttpResponse('you already registerd')
            else:
                data=registrationdata(mobile=mobile,otp=otp)
                data.save()
                return redirect("/otp/")
            regform=registrationform()
            return render(request,'registration.html',{'regform':regform})
        else:
            return HttpResponse('Please enter valid mobile number')
    else:
        regform = registrationform()
        return render(request, 'registration.html', {'regform': regform})
def otpverify(request):
    if request.method=="POST":
        otpf=otpform(request.POST)
        if otpf.is_valid():
            otp=request.POST.get('otp','')
            dat=registrationdata.objects.filter(otp=otp)
            if dat:
                dat.update(otp=1)
                return redirect("/service/")
            else:
                return HttpResponse("please enter valid otp")
            # otpf = otpform()
            # return render(request, 'otp.html', {'otpf': otpf})
        else:
            return HttpResponse('please enter valid otp')
    else:
        otpf=otpform()
        return render(request,'otp.html',{'otpf':otpf})


def serform(request):
    if request.method=="POST":
        sform=serviceform(request.POST)
        if sform.is_valid():
            name=request.POST.get('name','')
            service_name=request.POST.get('service_name','')
            gender=request.POST.get('gender','')
            mobile=request.POST.get('mobile','')
            password=request.POST.get('password','')
            conform_password=request.POST.get('conform_password','')
            shop_address=request.POST.get('shop_address','')
            data=registrationdata.objects.filter(mobile=mobile)
            if data:
                data.update(name=name,service_name=service_name,gender=gender,password=password,conform_password=conform_password,shop_address=shop_address)
            else:
                return HttpResponse('please enter same phone number')
            # sform=serviceform()
            # return render(request,'service.html',{'sform':sform})
            return redirect("/#/")
        else:
            return HttpResponse('Invalid details')
    else:
        sform = serviceform()
        return render(request, 'service.html', {'sform': sform})
# @login_required(login_url="/login/")
def rlogin(request):
    if request.method=="POST":
        lform=loginform(request.POST)
        if lform.is_valid():
            mobile=request.POST.get('mobile','')
            password=request.POST.get('password','')
            mob=registrationdata.objects.filter(mobile=mobile)
            pwd=registrationdata.objects.filter(password=password)
            if mob and pwd:
                return redirect("/admindata/")
            else:
                return HttpResponse('invalid username and password')
            # usr= authenticate(request,username=username,password=password)
            # if usr:
                # if request.GET.get('next',None):
                #     return redirect(request.GET['next'])

                # rform = cdata.objects.filter(username=username)
                # rform=cdata.objects.all()
                # return render(request,'details.html',{'rform':rform})

        else:
            return HttpResponse('invalid data')
    else:
        lform = loginform()
        return render(request, 'login.html', {'lform': lform})
def admindataa(request):
    data=userdata.objects.all()
    return render(request,'regdata.html',{'data':data})

def forgpass(request):
    if request.method=='POST':
        forform=forgotpassword(request.POST)
        if forform.is_valid():
            mobile=request.POST.get('mobile','')
            mob=registrationdata.objects.filter(mobile=mobile)
            if mob:
                mob.update(otp=otp)
                return redirect("/otp/")
            else:
                return HttpResponse('number not their')
            # forform = forgotpassword()
            # return render(request, 'forgotpassword.html', {'forform': forform})
        else:
            return HttpResponse('please enter valid number')
    else:
        forform=forgotpassword()
        return render(request,'forgotpassword.html',{'forform':forform})


def updatepass(request):
    if request.method=="POST":
        up=updatepassword(request.POST)
        if up.is_valid():
            password = request.POST.get('password', '')
            conform_password = request.POST.get('conform_password', '')
            mob.update(password=password,conform_password=conform_password)
        else:
            return HttpResponse('please enter valid password')
    else:
        up=updatepassword()
        return render(request,'updatepassword.html',{'up':up})

# def forotpverify(request):
#     if request.method=="POST":
#         otpf=otpform(request.POST)
#         if otpf.is_valid():
#             otp=request.POST.get('otp','')
#             dat=registrationdata.objects.filter(otp=otp)
#             if dat:
#                 dat.update(otp=1)
#                 return redirect("/service/")
#             else:
#                 return HttpResponse("please enter valid otp")
#             # otpf = otpform()
#             # return render(request, 'otp.html', {'otpf': otpf})
#         else:
#             return HttpResponse('please enter valid otp')
#     else:
#         otpf=otpform()
#         return render(request,'otp.html',{'otpf':otpf})

# def logoutt(request):
#     if request.method=="POST":
#         logout(request)
#         return render(request,'login.html')

