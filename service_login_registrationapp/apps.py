from django.apps import AppConfig


class ServiceLoginRegistrationappConfig(AppConfig):
    name = 'service_login_registrationapp'
