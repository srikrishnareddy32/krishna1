from django.db import models

class registrationdata(models.Model):
    mobile=models.BigIntegerField()
    otp=models.IntegerField()
    name=models.CharField(max_length=50)
    service_name=models.CharField(max_length=50)
    gender=models.CharField(max_length=50)
    password=models.CharField(max_length=50)
    conform_password=models.CharField(max_length=50)
    shop_address=models.CharField(max_length=200)

class userdata(models.Model):
    name = models.CharField(max_length=50)
    service_name = models.CharField(max_length=50)
    mobile = models.BigIntegerField()