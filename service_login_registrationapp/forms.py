from django import forms
class registrationform(forms.Form):
    mobile=forms.IntegerField(label='Enter your mobile number',widget=forms.NumberInput(attrs={'placeholder':'mobile','class':'form-control'}))
class otpform(forms.Form):
    otp=forms.IntegerField(label='Enter your otp',widget=forms.NumberInput(attrs={'placeholder':'OTP','class':'form-control'}))

class serviceform(forms.Form):
    name = forms.CharField(label='Enter your Name',widget=forms.TextInput(attrs={'placeholder':'Name','class':'form-control'}))
    service_name = forms.CharField(label='Enter your Service Name',widget=forms.TextInput(attrs={'placeholder':'Service Name','class':'form-control'}))
    gender_choice=(('male','male'),('Female','Female'))
    gender = forms.ChoiceField(widget=forms.RadioSelect(),choices=gender_choice)
    mobile = forms.IntegerField(label='Enter your Mobile',widget=forms.NumberInput(attrs={'placeholder':'Mobile','class':'form-control'}))
    password = forms.CharField(label='Enter your Password',widget=forms.PasswordInput(attrs={'placeholder':'Password','class':'form-control'}))
    conform_password = forms.CharField(label='Enter your conform_password',widget=forms.PasswordInput(attrs={'placeholder':'conf password','class':'form-control'}))
    shop_address = forms.CharField(label='Enter your address',widget=forms.TextInput(attrs={'placeholder':'address','class':'form-control'}))


class loginform(forms.Form):
    mobile = forms.IntegerField(label='Enter your Mobile',widget=forms.NumberInput(attrs={'placeholder':'Mobile','class':'form-control'}))
    password = forms.CharField(label='Enter your Password',widget=forms.PasswordInput(attrs={'placeholder':'Password','class':'form-control'}))


class forgotpassword(forms.Form):
    mobile = forms.IntegerField(label='Enter your Mobile',widget=forms.NumberInput(attrs={'placeholder':'Mobile','class':'form-control'}))


class updatepassword(forms.Form):
    password = forms.CharField(label='Enter your Password',widget=forms.PasswordInput(attrs={'placeholder':'Password','class':'form-control'}))
    conform_password = forms.CharField(label='Enter your conform_password',widget=forms.PasswordInput(attrs={'placeholder':'conf password','class':'form-control'}))

class forotpform(forms.Form):
    otp=forms.IntegerField(label='Enter your otp',widget=forms.NumberInput(attrs={'placeholder':'OTP','class':'form-control'}))
    username = forms.IntegerField(label='Enter your Mobile',widget=forms.NumberInput(attrs={'placeholder':'Mobile','class':'form-control'}))
    password = forms.CharField(label='Enter your Password',widget=forms.PasswordInput(attrs={'placeholder':'Password','class':'form-control'}))
